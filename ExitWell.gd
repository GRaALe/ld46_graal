extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

var hero_here = false
var creature_here = false

signal both_here

func _on_Area2D_area_entered(area):
	if area.get_parent().is_in_group("hero"):
		hero_here = true
	if area.get_parent().is_in_group("creature"):
		creature_here = true
	if hero_here and creature_here:
		emit_signal("both_here")


func _on_Area2D_area_exited(area):
	if area.get_parent().is_in_group("hero"):
		hero_here = false
	if area.get_parent().is_in_group("creature"):
		creature_here = false
