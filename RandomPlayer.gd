extends Timer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var can_play = true

export var timeout = 1.0

# Called when the node enters the scene tree for the first time.
func _ready():
	wait_time = timeout

func is_playing():
	for c in get_children():
		var audio: AudioStreamPlayer2D = c
		if audio.playing:
			return true
	return false

func play():
	if not can_play:
		return
	var rnd = randi() % get_child_count()
	for c in get_children():
		var audio: AudioStreamPlayer2D = c
		audio.stop()
	get_child(rnd).play()
	can_play = false
	start()
		


func stop():
	for c in get_children():
		var audio: AudioStreamPlayer2D = c
		audio.stop()



func _on_RandomPlayer_timeout():
	can_play = true
