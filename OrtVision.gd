extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var radius = 2
export var size = 1

export (Texture) var vision_texture
export (Color) var vision_color
export (bool) var reduce_vision = true

var Left = []
var Right = []
var Up = []
var Down = []

# Called when the node enters the scene tree for the first time.
func _ready():
	prepare_all()
	
func prepare_all():
	for c in get_children():
		if c is CollisionShape2D:
			c.queue_free()
	Left = []
	Right = []
	Up = []
	Down = []
	prepare(Left, Vector2(-1, 0))
	prepare(Right, Vector2(1, 0))
	prepare(Up, Vector2(0, -1))
	prepare(Down, Vector2(0, 1))
	
var is_blocked = false	

func update_with_direction(direction: Vector2, going_to_move = false):
	is_blocked = false
	set_enabled(Left, direction.x == -1, direction, going_to_move)
	set_enabled(Right, direction.x == 1, direction, going_to_move)
	set_enabled(Up, direction.y == -1, direction, going_to_move)
	set_enabled(Down, direction.y == 1, direction, going_to_move)
	
func update_all(flag):
	return [
		set_enabled(Left, flag, Vector2(-1, 0), false),
		set_enabled(Right, flag, Vector2(1, 0), false),
		set_enabled(Up, flag, Vector2(0, -1), false),
		set_enabled(Down, flag, Vector2(0, 1), false)
	]
	
func update_both_with_direction(direction: Vector2, going_to_move = false):
	is_blocked = false
	return [
		set_enabled(Left, direction.x != 0, Vector2(-1, 0), going_to_move),
		set_enabled(Right, direction.x != 0, Vector2(1, 0), going_to_move),
		set_enabled(Up, direction.y != 0, Vector2(0, -1), going_to_move),
		set_enabled(Down, direction.y != 0, Vector2(0, 1), going_to_move)
	]
		
	
func update_all_with_direction(direction: Vector2, going_to_move = false):
	set_enabled(Left, true, Vector2(-1, 0), going_to_move and direction.x == -1)
	set_enabled(Right, true, Vector2(1, 0), going_to_move and direction.x == 1)
	set_enabled(Up, true, Vector2(0, -1), going_to_move and direction.y == -1)
	set_enabled(Down, true, Vector2(0, 1), going_to_move and direction.y == 1)
		
	
func set_enabled(grp: Array, flag: bool, direction: Vector2, going_to_move = false):
	if not flag:
#		print("%s is off" % [direction])
		for c in grp:
			c.disabled = true
			c.visible = false
		return null
	else:
		var reduce_distance = 0
		if going_to_move:
			reduce_distance = 1
		$RayCast2D.cast_to = direction * (radius+1) * 16 * size
		$RayCast2D.force_raycast_update()
		var collider = $RayCast2D.get_collider()
		var max_distance = (2+radius)
		if collider != null:
			var point = $RayCast2D.get_collision_point()
			var max_cell_distance = round((point - global_position).length() / 16)
			max_distance = ceil(max_cell_distance / size)
			#if direction.x == -1:
			#	print("%s %s %f %d" % [ point, global_position, (point - global_position).length() , max_distance])
#				max_distance += 1
			#print("MD is %d" % max_distance)
			is_blocked = max_distance <= 1
			max_distance -= reduce_distance
		for c in grp:
			if c.position.length() < max_distance*16*size:
				c.disabled = false
				c.visible = true
			else:
				c.disabled = true
				c.visible = false
		return max_distance

func prepare(grp: Array, vec: Vector2):
	for i in range(radius):
		var shape = CollisionShape2D.new()
		shape.shape = RectangleShape2D.new()
		(shape.shape as RectangleShape2D).extents = Vector2(8, 8) * size
		add_child(shape)
		grp.append(shape)
		shape.position = vec*(1+i)*16 * size
		
		var sprite = Sprite.new()
		sprite.texture = vision_texture
		var color: Color = vision_color
		if reduce_vision:
			color.a = 0.5/pow(2, i*3/radius)
		sprite.self_modulate = color
		
		shape.add_child(sprite)
		sprite.position = Vector2.ZERO
		sprite.scale = Vector2(size, size)
		#sprite.position = vec*(1+i)*16

