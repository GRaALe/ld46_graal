extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


var creature
# Called when the node enters the scene tree for the first time.
func _ready():
	creature = get_tree().get_nodes_in_group("creature")[0]
	assert(creature != null)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var hp = creature.hp
	for i in range(1, $MarginContainer/VBoxContainer/HBoxContainer.get_child_count()):
		var c = $MarginContainer/VBoxContainer/HBoxContainer.get_child(i)
		if i <= hp:
			c.visible = true
		else:
			c.visible = false
			
