extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (Array, Texture) var drops

var dropped_drops = []

# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.


func drop_random(center: Vector2, direction: Vector2, count = 1):
	for i in range(count):
		var pos = center - global_position - Vector2(16,16) + direction.rotated(PI/2) * 12.0 * pow(rand_range(-1.0, 1.0), 1)
		dropped_drops.append([pos, randi()%drops.size()])
	update()
	
func _draw():
	for drop in dropped_drops:
		draw_texture(drops[drop[1]], drop[0])
