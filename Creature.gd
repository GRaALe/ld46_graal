extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var hp = 16.0
var MAX_HP = 16.0

var direction_to_hero = null
var health = 0
var is_in_turn = false

signal drop_blood
signal on_creature_dead

var drop_scene = preload("res://PDrops.tscn")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	$OrtVision.update_all(false)

func start_turn():
	if direction_to_hero != null:
		if hero_is_here:
			direction_to_hero = null
			$View.is_turning = false
			return
		is_in_turn = true
		$RayCast2D.cast_to = 24 * direction_to_hero
		$RayCast2D2.cast_to = 24 * direction_to_hero
		$RayCast2D3.cast_to = 24 * direction_to_hero
		$RayCast2D4.cast_to = 24 * direction_to_hero
		
		$RayCast2D.force_raycast_update()
		$RayCast2D2.force_raycast_update()
		$RayCast2D3.force_raycast_update()
		$RayCast2D4.force_raycast_update()
		
		if $RayCast2D.is_colliding() || $RayCast2D2.is_colliding() || $RayCast2D3.is_colliding() || $RayCast2D4.is_colliding():
			# todo: play sad noise
			is_in_turn = false
			direction_to_hero = null
			$View.is_turning = false

		else:
			$Tween.interpolate_property(self, "position", 
				position, position+32*direction_to_hero, 
				0.7, Tween.TRANS_QUAD, Tween.EASE_OUT)
			$Tween.start()
			var drop = drop_scene.instance()
			get_parent().add_child(drop)
			drop.position = self.position
			$View.is_turning = true

			
func is_going():
	return direction_to_hero != null
	
func is_turn_finished():
	return not is_in_turn
	
func process_turn():
	
	pass

func try_get_here(hero: Node2D):
	if hero_is_here:
		return false
	
	direction_to_hero = Vector2.ZERO
	var dx = round((hero.position.x - position.x)/32)
	var dy = round((hero.position.y - position.y)/32)
	
	
	if dx > 0:
		direction_to_hero.x = 1
	elif dx < 0:
		direction_to_hero.x = -1
	elif dy > 0:
		direction_to_hero.y = 1
	elif dy < 0:
		direction_to_hero.y = -1
	else:
		return false #???
	return true

func _process(delta):
	if hp <= 0:
		return
	var old_h = health
	health = hp/MAX_HP
	if health != old_h:
		$View.health = health


func play_animation_death():
	#self_modulate = Color.transparent
	$View.visible = false
	$Death.emitting = true
	#$Eye.visible = false


var hero_is_here = false

func _on_Area2D_area_entered(area):
	if area.get_parent().is_in_group("hero"):
		hero_is_here = true
	if area.get_parent().is_in_group("enemy"):
		if hp > 0:
			die()
		$Area2D.collision_mask = 0
		$Area2D.collision_layer = 0



func _on_Area2D_area_exited(area):
	if area.get_parent().is_in_group("hero"):
		hero_is_here = false


func _on_Tween_tween_all_completed():
	is_in_turn = false
	hp -= 1
	if hp == 0:
		emit_signal("on_creature_dead")


func _on_Tween_tween_step(object, key, elapsed, value):
	$Step.play()


func _on_DropBlood_timeout():
	if $Tween.is_active():
		emit_signal("drop_blood")


func _on_OrtVision_area_entered(area):
	if area.get_parent().is_in_group("enemy"):
		area.get_parent().die()

func add_hp():
	if hp < MAX_HP:
		hp += 2

var is_firing = false

func do_fire():
	$Fire.play()
	$View.is_turning = true
	var distances = $OrtVision.update_all(true)
	$AfterFireTimer.start()
	is_firing = true
	var camera = get_parent().get_node("Camera2D")
	$CameraTween.interpolate_property(camera, "rotation", 0, 0.05, 0.05, Tween.TRANS_BOUNCE)
	$CameraTween.interpolate_property(camera, "rotation", 0.05, -0.1, 0.05, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT, 0.05)
	$CameraTween.interpolate_property(camera, "rotation", -0.1, 0, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT, 0.1)
	$CameraTween.start()
	print(distances)
	$Fires/LeftFire.process_material.initial_velocity = distances[0] * 32 - 32
	$Fires/RightFire.process_material.initial_velocity = distances[1] * 32 - 32
	$Fires/UpFire.process_material.initial_velocity = distances[2] * 32 - 32
	$Fires/DownFire.process_material.initial_velocity = distances[3] * 32 - 32
	
	$Fires/LeftFire.emitting = true
	$Fires/RightFire.emitting = true
	$Fires/UpFire.emitting = true
	$Fires/DownFire.emitting = true
	
func die():
	if hp > 0:
		hp = 0
		emit_signal("on_creature_dead")
		$Area2D.collision_mask = 0
		$Area2D.collision_layer = 0

func _on_AfterFireTimer_timeout():
	$Fires/LeftFire.emitting = false
	$Fires/RightFire.emitting = false
	$Fires/UpFire.emitting = false
	$Fires/DownFire.emitting = false
	$OrtVision.update_all(false)
	is_firing = false
	$View.is_turning = false

	hp -= 2
	if hp <= 0:
		emit_signal("on_creature_dead")
	
