extends AnimatedSprite


export var is_opened = false
export var door_id = 4

func _ready():
	if is_opened:
		animation = "open"
		frame = 4
		$StaticBody2D.collision_layer = 0
	
func toggle():
	if is_opened:
		close()
	else:
		open()
	
func open():
	is_opened = true
	play("open")
	#TODO: why does not work?
	#$StaticBody2D/CollisionShape2D.disabled = true
	$StaticBody2D.collision_layer = 0
	$Open.play()
	
func close():
	is_opened = false
	play("open", true)
	#$StaticBody2D/CollisionShape2D.disabled = false
	$StaticBody2D.set_collision_layer_bit(1, true)
	$Close.play()
