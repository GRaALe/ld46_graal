extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var direction = Vector2(0, 1)
var is_warned = false

var path: Path2D
var current_point_idx = -1

var target_point = null
var start_position = null

var is_in_turn = false

var last_seen = null

export (int) var vision = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	$OrtVision.radius = vision
	$OrtVision.prepare_all()
	$OrtVision.update_with_direction(direction)
	path = get_node("Path2D")
	if path != null:
		start_position = path.global_position
		current_point_idx = 0

var recent_footprint_turn = -999

var ort_inited = false

func _process(delta):
	if not ort_inited:
		$OrtVision.update_with_direction(direction)
		ort_inited = true

func start_turn():
	if not is_warned:
		if path:
			current_point_idx = (current_point_idx+1)%path.curve.get_point_count()
			target_point = start_position + path.curve.get_point_position(current_point_idx)
	else:
		print("last seen is %s" % last_seen_hero_or_creature)
		if last_seen_hero_or_creature:
			if position == last_seen_hero_or_creature: # or (last_seen_hero_or_creature-position).length() > 64:
				last_seen_hero_or_creature = null
		target_point = null

		var hero = null
		var creature = null
		var more_recent_footprint = null
		
#		print(objects_visible)
		for visible in objects_visible:
			if visible.is_in_group("hero"):
				hero = visible.global_position
				last_seen_hero_or_creature = visible.global_position
				break
			elif visible.is_in_group("creature"):
				creature = visible.global_position
				last_seen_hero_or_creature = visible.global_position
			elif visible.is_in_group("drop"):
				print("here is drop %d, my known is %d" % [visible.turn_created, recent_footprint_turn])
				if visible.turn_created >= recent_footprint_turn:
					recent_footprint_turn = visible.turn_created
					more_recent_footprint = visible.global_position
					
		if hero:
			target_point = hero
			print("Follow hero")
			if (target_point - position).length() <= 32:
				$Attach.play()
		elif creature:
			target_point = creature
			print("Follow creature")
			if (target_point - position).length() <= 32:
				$Attach.play()
		elif more_recent_footprint:
			target_point = more_recent_footprint
			print("Follow footprint")
		elif last_seen_hero_or_creature:
			target_point = last_seen_hero_or_creature
			print("Follow last seen point")
		if target_point and (target_point - position).length() < 16:
			target_point = null

		#TBD
		
	var old_direction = direction
	if target_point != null:
		direction = (target_point - position).normalized()
		if abs(direction.x) > abs(direction.y):
			direction.x = sign(direction.x)
			direction.y = 0
		else:
			direction.y = sign(direction.y)
			direction.x = 0
	else:
		direction = direction.rotated(PI/2)
		direction.x = round(direction.x)
		direction.y = round(direction.y)

		
	$OrtVision.update_with_direction(direction, target_point != null)
	if target_point and not $OrtVision.is_blocked:
		is_in_turn = true
		var speed = 0.1
		if Global.current_level == Global.ENDING:
			speed = 0.05
		$Tween.interpolate_property(self, "position", position, position+direction*16, speed)
		$Tween.start()
		
		
		
func is_turn_finished():
	return not is_in_turn
	
func process_turn():
	pass




func _on_IdleTimer_timeout():
	$Eye.play("morg")


var objects_visible = []

var last_seen_hero_or_creature = null

func _on_OrtVision_area_entered(area):
	if not area.is_in_group("drop"):
		area = area.get_parent()
	objects_visible.append(area)
	if area.is_in_group("hero") or area.is_in_group("creature"):
		last_seen_hero_or_creature = area.global_position
	$Growl.play()
	is_warned = true
	
	$Eye.play("idle-warn")
	$Eye/Pupil.visible = true
	
	$IdleTimer.stop()
	
func _on_OrtVision_area_exited(area):
	if not area.is_in_group("drop"):
		area = area.get_parent()

	objects_visible.erase(area)


func _on_Tween_tween_all_completed():
	is_in_turn = false


func die():
	var d = $Death
	remove_child(d)
	get_parent().add_child(d)
	d.global_position = global_position
	d.emitting = true
	queue_free()
