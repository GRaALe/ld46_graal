extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (int) var vision = 3
export var direction = Vector2(0, 1)

var is_firing = false

var direction_change_to = 0

func _ready():
	$OrtVision.radius = vision
	$OrtVision.prepare_all()
	$OrtVision.update_both_with_direction(direction)

func start_turn():
	if objects_visible.size() > 0:
		fire()
	else:
		if direction_change_to > 0:
			direction_change_to -= 1
		else:
			direction_change_to = 0
			direction = direction.rotated(PI/2)
			direction.x = round(direction.x)
			direction.y = round(direction.y)
		$OrtVision.update_both_with_direction(direction, false)
	#print(direction)

func is_turn_finished():
	return not is_firing
	
func process_turn():
	pass
	
func fire():
	$Fire.play()
	$AfterFireTimer.start()
	$MidFireTimer.start()
	is_firing = true
	var camera = get_parent().get_node("Camera2D")
	$CameraTween.interpolate_property(camera, "rotation", 0, 0.05, 0.05, Tween.TRANS_BOUNCE)
	$CameraTween.interpolate_property(camera, "rotation", 0.05, -0.1, 0.05, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT, 0.05)
	$CameraTween.interpolate_property(camera, "rotation", -0.1, 0, 0.1, Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT, 0.1)
	$CameraTween.start()

	var distances = $OrtVision.update_both_with_direction(direction, false)
	
	if direction.x != 0:
		$Fires/LeftFire.process_material.initial_velocity = distances[0] * 32 - 32
		$Fires/RightFire.process_material.initial_velocity = distances[1] * 32 - 32
		
		$Fires/LeftFire.emitting = true
		$Fires/RightFire.emitting = true
	else:
		$Fires/UpFire.process_material.initial_velocity = distances[2] * 32 - 32
		$Fires/DownFire.process_material.initial_velocity = distances[3] * 32 - 32
	
		$Fires/UpFire.emitting = true
		$Fires/DownFire.emitting = true
	


var objects_visible = []


func _on_OrtVision_area_entered(area):
	if not area.is_in_group("drop"):
		area = area.get_parent()
		$Growl.play()
		objects_visible.append(area)


func _on_OrtVision_area_exited(area):
	if not area.is_in_group("drop"):
		area = area.get_parent()

		objects_visible.erase(area)



func _on_AfterFireTimer_timeout():
	#print(objects_visible)
	$Fires/LeftFire.emitting = false
	$Fires/RightFire.emitting = false
	$Fires/UpFire.emitting = false
	$Fires/DownFire.emitting = false
	is_firing = false



func _on_MidFireTimer_timeout():

	for o in objects_visible:
		o.die()	
