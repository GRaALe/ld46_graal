extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	modulate = Color.black
	$Center/VBoxContainer/Result1.visible = false
	$Center/VBoxContainer/Result2.visible = false
	$Center/VBoxContainer/Result3.visible = false
	if Global.hero_dead_at_the_end:
		$Center/VBoxContainer/Result3.visible = true
	elif Global.creature_in_safety and Global.creature_is_locked:
		$Center/VBoxContainer/Result2.visible = true
	else:
		$Center/VBoxContainer/Result1.visible = true

	$Tween.interpolate_property(self, "modulate", Color.black, Color.white, 2)
	$Tween.interpolate_property($TextureRect, "self_modulate", Color.transparent, Color.white, 1, Tween.TRANS_LINEAR, Tween.EASE_IN, 3)
	$Tween.start()

func _process(delta):
	if Input.is_action_just_pressed("fire"):
		Global.start_level(Global.ENDING)
	elif Input.is_action_just_pressed("whistle"):
		Global.start_level(0)
