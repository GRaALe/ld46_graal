extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal on_real_exit


func _on_Area2D_area_entered(area):
	emit_signal("on_real_exit")
