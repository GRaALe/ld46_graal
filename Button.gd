extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var is_opened = false
export var door_id = 4

var our_door = null

# Called when the node enters the scene tree for the first time.
func _ready():
	for door in get_tree().get_nodes_in_group("door"):
		if door.door_id == door_id:
			our_door = door
			self_modulate = our_door.self_modulate
			is_opened = door.is_opened
	if is_opened:
		play("opened")
	else:
		play("closed")



func toggle():
	if is_opened:
		close()
	else:
		open()

func open():
	is_opened = true
	play("opened")
	if our_door:
		our_door.open()

func close()	:
	is_opened = false
	play("closed")
	if our_door:
		our_door.close()


func _on_Area2D_area_entered(area):
	toggle()
