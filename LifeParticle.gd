extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var my_offset

var is_wounded = false setget set_wounded

func set_wounded(val):
	is_wounded = val
	if is_wounded:
		self_modulate = 	Color("78570202")
#		self_modulate.a = 0.8
		#rotation_degrees = 45
		$AnimationPlayer.stop()
		stop()
		play("wounded")
	else:
		rotation_degrees = 0
		stop()		

# Called when the node enters the scene tree for the first time.
func _ready():
	set_wounded(is_wounded)
	self_modulate = Color.transparent
	my_offset = position
	$Timer.wait_time = rand_range(3, 4)
	$Timer.start()


func run_animation():
	frame = 0
	if is_wounded:
		play("wounded")
	else:
		play("simple")


func _on_Timer_timeout():
	if is_wounded:
		return
	$AnimationPlayer.play("flash")
