extends AnimatedSprite

var is_turn = false
var await_input = false
var creature = null

func _ready():
	creature = get_tree().get_nodes_in_group("creature")[0]
	assert(creature != null)

func start_turn():
	if creature.is_going():
		return
	is_turn = true
	await_input = true
	
func is_turn_finished():
	return not is_turn
	
func play_animation_death():
	self_modulate = Color.transparent
	$Eye.visible = false
	$Death.emitting = true
	
	
var await_fire_end = false

func on_bonus():
	creature.add_hp()
	$Bonus.play()

func process_turn():
	if await_fire_end:
		if creature.is_firing:
			return
		else:
			is_turn = false
			await_fire_end = false
			return
	if not await_input:
		return
	var check_direction = Vector2.ZERO
	if Input.is_action_pressed("whistle"):
		if creature.hp > 0 and creature.try_get_here(self):
			$WhistleSound.play()
			await_input = false
			is_turn = false
			#stop_animations()
		else:
			$Nonono.play()
	elif Input.is_action_pressed("fire"):
		if creature.hp > 0:
			creature.do_fire()
			await_input = false
			await_fire_end = true
		else:
			$Nonono.play()
	elif Input.is_action_pressed("ui_left"):
		check_direction.x = -1
	elif Input.is_action_pressed("ui_right"):
		check_direction.x = 1
	elif Input.is_action_pressed("ui_up"):
		check_direction.y = -1
	elif Input.is_action_pressed("ui_down"):
		check_direction.y = 1
		
	if check_direction != Vector2.ZERO:
		$RayCast2D.cast_to = check_direction * 8
		$RayCast2D.force_raycast_update()
		var on_way = $RayCast2D.get_collider()
		if on_way:
			$Nonono.play()
		else:
			await_input = false
			var target_pos = position + check_direction * 16
			$Tween.interpolate_property(self, "position", position, target_pos, 0.1)
			$Tween.start()

func _on_IdleAnim_timeout():
	if is_turn:
		$Eye/Pupil.visible = false
		$Eye.play("morg")


func _on_Eye_animation_finished():
	if $Eye.animation == "morg":
		stop_morging()
		
func stop_morging():
	$Eye.play("idle")
	$Eye/Pupil.visible = true
	
func stop_animations():
	$Eye.play("waiting")
	$Eye/Pupil.visible = false
	


func _on_Tween_tween_all_completed():
	is_turn = false
	#stop_animations()


func _on_Tween_tween_step(object, key, elapsed, value):
	$Step.play()

signal hero_dead


var is_dead = false

func die():
	if not is_dead:
		is_dead = true
		emit_signal("hero_dead")
	

func _on_Area2D_area_entered(area):
	if area.get_parent().is_in_group("enemy"):
		die()
