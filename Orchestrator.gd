extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func finish_level():
	$Exit.play()
	$NextTimer.start()
	
	var creature = get_parent().get_node("Creature")
	var hero = get_parent().get_node("Hero")
	var exit_well = get_parent().get_node("ExitWell")
	var ls: Sprite = exit_well.get_node("LightSphere")
	ls.visible = true
	$Tween.interpolate_property(ls, "scale", Vector2(0.01, 0.01), Vector2(2,2), 2)
	$Tween.interpolate_property(hero, "position", hero.position, hero.position + Vector2(0, -100), 1)
	$Tween.interpolate_property(creature, "position", creature.position, creature.position + Vector2(0, -150), 1)
	$Tween.interpolate_property(hero, "modulate", Color.white, Color.transparent, 1)
	$Tween.interpolate_property(creature, "modulate", Color.white, Color.transparent, 1)
	$Tween.interpolate_property(creature, "hp", creature.hp, creature.MAX_HP, 0.3)

	$Tween.interpolate_property(get_parent(), "modulate", Color.white, Color.black, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 1)	
	$Tween.start()

func on_hero_reached_real_exit():
	if not get_parent().get_node("SafeDoor").is_opened:
		Global.creature_is_locked = true
	$Exit.play()
	$NextTimer.start()
	$Tween.interpolate_property(get_parent(), "modulate", Color.white, Color.black, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 1)	
	$Tween.start()
	
	
func on_creature_death_in_finale():
	$Death.play()
	var creature = get_parent().get_node("Creature")
	creature.play_animation_death()
	Global.creature_dead_at_the_end = true
	
func on_ending():
	fade_out()
	$NextTimer.start()
	
func on_creature_death():
	$Death.play()
	var creature = get_parent().get_node("Creature")
	creature.play_animation_death()
	fade_out()
	$RestartTimer.start()
	
func on_hero_death():
	$Death.play()
	var hero = get_parent().get_node("Hero")
	hero.play_animation_death()
	if Global.current_level == Global.ENDING:
		Global.hero_dead_at_the_end = true
		on_ending()
		return
	
	fade_out()
	$RestartTimer.start()
	
func restart():
	get_parent().in_loop = false
	$Death.play()
	fade_out()
	$RestartTimer.start()
	
	

func fade_out():
	$Tween.interpolate_property(get_parent(), "modulate", Color.white, Color.black, 0.5)	
	$Tween.start()
	
var is_fading_in = false

func fade_in():
	is_fading_in = true
	$Tween.interpolate_property(get_parent(), "modulate", Color.black, Color.white, 0.5)	
	$Tween.start()
	
func _on_RestartTimer_timeout():
	get_tree().reload_current_scene()


func _on_Tween_tween_completed(object, key):
	if is_fading_in:
		is_fading_in = false
		get_parent().after_fade_in()


func _on_NextTimer_timeout():
	Global.next_level()
