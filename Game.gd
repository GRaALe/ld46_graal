extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"



var turnables = []
var this_turn = []
var current = null

var in_loop = false

export var level_nr = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	modulate = Color.black
	Global.current_level = level_nr
	
	for t in get_tree().get_nodes_in_group("turn-based"):
		turnables.append(t)
		var node: Node2D = t
		node.connect("tree_exiting", self, "on_turnable_exit", [node])
	
	$ExitWell.connect("both_here", self, "on_both_in_exit")
	$Creature.connect("drop_blood", self, "on_drop_blood")
	$Creature.connect("on_creature_dead", self, "on_creature_dead")
	$Hero.connect("hero_dead", self, "on_hero_dead")
	
	connect("tree_exiting", self, "on_tree_exiting")
	
	var real_exit = get_node("RealExit")
	if real_exit:
		real_exit.connect("on_real_exit", self, "on_real_exit")
	
	$Orchestrator.fade_in()
	
	if level_nr == 0:
		$Creature.hp = 7.0
		$UI/Tween.interpolate_property($UI/Hint, "modulate", Color.transparent, Color.white, 4, Tween.TRANS_LINEAR, Tween.EASE_IN, 1.5)
		$UI/Tween.start()
	if level_nr == Global.ENDING:
		Global.creature_dead_at_the_end = false
		Global.hero_dead_at_the_end = false
		Global.creature_in_safety = false
		Global.creature_is_locked = false
	
func on_real_exit():
	in_loop = false
	current = null
	$Orchestrator.on_hero_reached_real_exit()
	
	
func on_tree_exiting():
	in_loop = false
	current = null
	
func after_fade_in():
	in_loop = true
	for t in turnables:
		if t.has_method("before_all"):
			t.before_all()
	on_end_turn()
	
func on_hero_dead():
	in_loop = false
	current = null
	$Orchestrator.on_hero_death()
	
func on_creature_dead():
	print("%d == %d" % [level_nr, Global.ENDING])
	if level_nr == Global.ENDING:
		$Orchestrator.on_creature_death_in_finale()
		return
	in_loop = false
	current = null
	$Orchestrator.on_creature_death()
	
func on_drop_blood():
	$DecalableFloor.drop_random($Creature.global_position, $Creature.direction_to_hero, 1+(1-$Creature.health)*3)

var both_in_exit = false

func on_both_in_exit():
	both_in_exit = true

func on_turnable_exit(node):
	if not in_loop:
		return
	turnables.remove(turnables.find(node))
	var idx = this_turn.find(node)
	if idx != -1:
		this_turn.remove(idx)
	if turnables.size() == 0:
		in_loop = false
		current = null
	if current == node:
		on_end_turn()

func on_end_turn():
	if both_in_exit:
		in_loop = false
		current = null
		$Orchestrator.finish_level()
		return
	if this_turn.size() == 0:
		Global.current_turn += 1
		#print("new turn for all")
		for c in turnables:
			this_turn.append(c)
	current = this_turn.pop_front()
	current.start_turn()
	#print("%s started turn" % current.name)


func _process(delta):
	if not current or not in_loop:
		return
	if Input.is_action_just_pressed("restart"):
		$Orchestrator.restart()
	current.process_turn()
	if current.is_turn_finished():
		#print("%s ended turn" % current.name)
		on_end_turn()



func _on_SafePlace_area_entered(area):
	Global.creature_in_safety = true
