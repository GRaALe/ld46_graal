extends Node

var current_turn = 0

var levels = [
	preload("res://Level0.tscn"),
	preload("res://Level1.tscn"),
	preload("res://Level2.tscn"),
	preload("res://Level3.tscn"),
	preload("res://Level4.tscn"),
	preload("res://Level5.tscn"),
	preload("res://Level_End.tscn"),
	preload("res://Ending.tscn")
]

var current_level = 0

var ENDING = levels.size() - 2


var creature_dead_at_the_end = false
var hero_dead_at_the_end = false
var creature_in_safety = false
var creature_is_locked = false

func start_level(nr):
	current_level = nr
	start()

func start():
	get_tree().change_scene_to(levels[current_level])


func next_level():
	current_level += 1
	start()


