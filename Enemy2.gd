extends "res://Enemy.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	._ready()
	is_warned = true
	
	$Eye.play("idle-warn")
	$Eye/Pupil.visible = true
	$Eye2.play("idle-warn")
	$Eye2/Pupil.visible = true
	$Eye3.play("idle-warn")
	$Eye3/Pupil.visible = true
	$Eye4.play("idle-warn")
	$Eye4/Pupil.visible = true
	
	$OrtVision.update_all_with_direction(direction, false)

var ort_inited2 = false

func _process(delta):
	if not ort_inited2:
		$OrtVision.update_all_with_direction(direction, false)
		ort_inited2 = true

func start_turn():
	.start_turn()
	$OrtVision.update_all_with_direction(direction, target_point != null)
	

func rand_coord():
	return (randi()%3)-1

func _on_EyeTimer_timeout():
	$Eye/Pupil.position = Vector2(rand_coord(), rand_coord())
	$Eye2/Pupil.position = Vector2(rand_coord(), rand_coord())
	$Eye3/Pupil.position = Vector2(rand_coord(), rand_coord())
	$Eye4/Pupil.position = Vector2(rand_coord(), rand_coord())

