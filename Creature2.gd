extends AnimatedSprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (float) var health = 1.0 setget set_health
export (bool) var is_turning = false setget set_turning

func set_turning(val):
	if is_turning == val:
		return
	is_turning = val
	if is_turning:
		$Eye/Tear.stop()
		$Eye/Tear.visible = false
		$Eye.play("turn")
	else:
		$Eye.play("idle")

func set_health(val):
	health = val
	if not $LifeParticles:
		return
	var visible_only_before_idx = floor(val*$LifeParticles.get_child_count())

	for i in range($LifeParticles.get_child_count()):
		$LifeParticles.get_child(i).is_wounded = i >= visible_only_before_idx	
	
	$BreathTimer.wait_time = 0.1 + health * 10.0

# Called when the node enters the scene tree for the first time.
func _ready():
	set_turning(is_turning)
	set_health(health)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BreathTimer_timeout():
	if health < 0.5:
		play("breath-wounded")
	else:
		play("breath")


func _on_TearTimer_timeout():
	if is_turning:
		return
	$Eye/Tear.frame = 0
	$Eye/Tear.visible = true
	$Eye/Tear.play("tear")


func _on_Tear_animation_finished():
	$Eye/Tear.stop()
	$Eye/Tear.visible = false


func _on_Creature_animation_finished():
	play("idle")
