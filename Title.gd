extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_AnimatedSprite_animation_finished():
	$AnimatedSprite.play("idle")
	$AnimatedSprite.play("beat")

var starting = false

func _input(event):
	if starting:
		return
	if event is InputEventKey or event is InputEventMouseButton or event is InputEventJoypadButton:
		starting = true
		$Tween.interpolate_property(self, "modulate", Color.white, Color.black, 1.5)
		$Tween.start()


func _on_Tween_tween_all_completed():
	Global.start()
